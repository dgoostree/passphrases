package com.goostree.codewars.passphrases;

public class PlayPass {
	private static final int ASCII_A = 65;
	private static final char ASCII_0 = '0';
	private static final char NINE = '9';
	
	public static String playPass(String s, int n) {
		StringBuilder builder = new StringBuilder();
		
		for(int i = 0; i < s.length(); i++) {
			builder.append(transform(s.charAt(i), n, i));
		}
		
		return builder.reverse().toString();
	}
	
	private static char transform(char c, int n, int index) {
		if(Character.isDigit(c)) {
			return (char) ((NINE - c) + ASCII_0);
		} else if(Character.isAlphabetic(c)) {
			char ch = (char) ((c + n - ASCII_A) % 26 + ASCII_A);
			return (index % 2) == 1 ? Character.toLowerCase(ch) : Character.toUpperCase(ch);
		} else {
			return c;
		}
	}

}
